$cwd = Get-Location
$array = @()

function processApp($app, $path_spec="") {
  if ($path_spec -eq "") {
    $json = Get-Content -Path ./investigate.json | ConvertFrom-Json
    $rec = $json.software.$app
    $rec | Add-Member -MemberType NoteProperty -Name 'name' -Value $app
    $global:array += $rec
    return 0
  }
  Set-Location $global:cwd
  scoop install $app
  scoop update $app
  $version = ( scoop-console-x86_64-static.exe --latest $app | %{ $_.Split(" ")[0] } )
  Write-Output $version
  $path= ( scoop-console-x86_64-static.exe --latest $app | %{ $_.Split(" ")[1] } )
  Write-Output $path
  $url = "https://gitlab.com/spider-explorer/spider-software-2/-/raw/main/.software/${app}-${version}.7z"
  Write-Output $url
  $rec = [pscustomobject]@{name=$app; version=$version; ext="7z"; "path"=$path_spec; "url"=$url}
  $json = $rec | ConvertTo-Json
  Write-Output $json
  $global:array += $rec
  $f = "$global:cwd/.software/$app-$version.7z"
  if (Test-Path $f) {
    Write-Host $f is found!
    return 0
  }
  if (Test-Path $path) {
    Set-Location $global:cwd
    New-Item -Path .software -ItemType Directory -Force
    Set-Location $path
    if (Test-Path "IDE/bin/idea.properties") {
      sed -i -e "s/^idea[.]config[.]path=/#\\0/g" -e "s/^idea[.]system[.]path=/#\\0/g" -e "s/^idea[.]plugins[.]path=/#\\0/g" -e "s/^idea[.]log[.]path=/#\\0/g" IDE/bin/idea.properties
    }
    7z.exe a -r $global:cwd/.software/$app-$version.7z * -x!"User Data" -x!"profile" -x!"distribution"
  }
  return 0
}

scoop install git
scoop bucket add main
scoop bucket add extras
scoop bucket add java
$json = Get-Content -Path ./investigate.json | ConvertFrom-Json
$json | ConvertTo-Json
$json.software.msys2 | ConvertTo-Json
foreach( $key in $json.software.psobject.properties.name )
{
    #Write-Host $key ' = ' $configData.common.config.$key
    Write-Host $key
}
processApp busybox
processApp bluegriffon
processApp PowerShell
processApp grepwin "."
processApp idea "/IDE/bin"
processApp rider
processApp dotnet-sdk "."
processApp nuget "."
processApp ilmerge "."
processApp httrack "."
processApp sumatrapdf "."
processApp qt-creator "/bin"
processApp vim "."
processApp dart "/bin"
processApp racket "."
processApp zig "."
processApp tidy "."
processApp xmllint "/bin"
processApp nodejs-lts "."
processApp zulu17-jdk "/bin"
processApp gradle "/bin"
processApp nu "."
processApp sliksvn "/bin"
processApp gh "/bin"
processApp googlechrome "."
processApp wixtoolset "."
processApp uncrustify "/bin"
processApp geany "/bin"
processApp deno "."
processApp vscode "/bin;."
processApp file "."
processApp astyle "/bin"
processApp 7zip "."
processApp nyagos "."
processApp jq "."
processApp windows-terminal "."
processApp sqlitestudio "."
processApp rapidee "."
processApp notepad3 "."
processApp smartgit "/bin"
processApp wget "."
processApp curl "/bin"
processApp qownnotes "."
processApp sed "."
processApp msys2
processApp ClickOnce
processApp libreoffice "/LibreOffice/program"
processApp winpython
processApp git "/cmd"
$array.Length
$array | ConvertTo-Json
$result = [pscustomobject]@{software=$array}
$result | ConvertTo-Json | Set-Content -Path software-array.json
dos2unix software-array.json
