#! bash -uvx
rm -rf busybox-tilde64.7z
rm -rf busybox.tmp
mkdir -p busybox.tmp
./busybox_tilde64.exe --install ./busybox.tmp
cd busybox.tmp
rm -rf ar.exe strings.exe unzip.exe sh.exe sed.exe wget.exe
7z a -r ../busybox-tilde64.7z *
